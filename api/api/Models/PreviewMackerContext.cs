using Microsoft.EntityFrameworkCore;
using api.Models;

namespace api.Models 
{
    public class PreviewMakerContext : DbContext 
    {
        public PreviewMakerContext() { }
        public PreviewMakerContext(DbContextOptions<PreviewMakerContext> options) : base (options) { }
        public DbSet<Download> Downloads { get; set; }
    }
}