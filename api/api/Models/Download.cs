using System;

namespace api.Models 
{
    public class Download
    {
        public int ID { get; set; }
        public DateTime DownloadAt { get; set; }
    }
}